function rotation(arr) {
	
	var result = new Array(arr.length);

	for(var i = 0; i < arr.length; i++){
		result[i] = new Array(arr[i].length);
		for(var j = 0; j < arr[i].length; j++){
			result[i][j] = arr[j][i];
		}
		result[i].reverse();
	}
	return result;
}


var array = [
		[1,   2,  3,  4],
		[5,   6,  7,  8],
		[9,  10, 11, 12],
		[13, 14, 15, 16]
];

// var time = performance.now()
console.log(rotation(array));
// time = performance.now() - time;
// console.log("Time " + time);